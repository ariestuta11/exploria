@extends('../admin')

@section('konten')
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Add Bookings Data<small>exploria</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @if (session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
                @endif
                <form action="/addbookings" method="POST">
                    <p>For adding bookings data in <code>exploria</code> correctly at <a href="form.html"> form page</a></p>
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="nama_pengunjung">Visitors Name</label>
                            <input type="text" class="form-control" id="nama_pengunjung" name="nama_pengunjung" required="required">
                        </div>
                        <div class="form-group">
                            <label for="nama_hotel">Hotel</label>
                            <input type="text" class="form-control" id="nama_hotel" name="nama_hotel" required="required">
                        </div>
                        <div class="form-group">
                            <label for="malam">How Night?</label>
                            <input type="text" class="form-control" id="malam" name="malam" required="required">
                        </div>
                        <div class="form-group">
                            <label for="no_telp">No Telp</label>
                            <input type="text" class="form-control" id="no_telp" name="no_telp" required="required">
                        </div>
                        <div class="form-group">
                            <label for="alamat">Address</label>
                            <input type="text" class="form-control" id="alamat" name="alamat" required="required">
                        </div>
                        <div class="form-group">
                            <label for="keterangan_pengunjung">Notes</label>
                            <input type="text" class="form-control" id="keterangan_pengunjung" name="keterangan_pengunjung" required="required">
                        </div>
                    </div>

                    <div class=" ml-3 mr-3 mb-1">
                        <button type="submit" class="btn btn-success">Add Bookings</button>

                        <a class="btn btn-secondary" href="/allbookings" role="button">See All Bookings Data</a>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>


@endsection()