@extends('../admin')

@section('konten')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<h2>Ubah Data Pengunjung: {{$pilihstaff->nama_staff}}</h2>
<form action="/lihatstaff/{{$pilihstaff->id_staff}}" method="POST">
    @csrf
    @method('PUT')
    <div class="card-body">
        <div class="form-group">
            <label for="nama_staff">Nama</label>
            <input type="text" class="form-control" id="nama_staff" name="nama_staff" value="{{$pilihstaff->nama_staff}}">
        </div>
        <div class="form-group">
            <label for="nama_hotel">Hotel</label>
            <input type="text" class="form-control" id="nama_hotel" name="nama_hotel" value="{{$pilihstaff->nama_hotel}}">
        </div>
        <div class="form-group">
            <label for="keterangan_staff">Keterangan</label>
            <input type="text" class="form-control" id="keterangan_staff" name="keterangan_staff" value="{{$pilihstaff->keterangan_staff}}">
        </div>
    </div>
    <div class="card ml-3 mr-3 mb-1">
        <button type="submit" class="btn btn-primary btn-user btn-block" value="Ubah Data">Ubah Data</button>
    </div>
</form>
@endsection
<!-- I Made Kamajaya Ariestuta (203140714111006) -->