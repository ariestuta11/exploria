@extends('../admin')

@section('konten')
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Add Facility Data<small>exploria</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @if (session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
                @endif
                <form action="/addfacility" method="POST" enctype="multipart/form-data">
                    <p>For adding facility data in <code>exploria</code> correctly at <a href="form.html"> form page</a></p>
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="nama_fasilitas">Facility</label>
                            <input type="text" class="form-control" id="nama_fasilitas" name="nama_fasilitas" required="required">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlFile1">Upload Facility Image</label>
                            <input type="file" class="form-control-file" id="gambar_fasilitas" name="gambar_fasilitas">
                        </div>
                        <div class="form-group">
                            <label for="keterangan_fasilitas">Notes</label>
                            <input type="text" class="form-control" id="keterangan_fasilitas" name="keterangan_fasilitas" required="required">
                        </div>
                    </div>

                    <div class=" ml-3 mr-3 mb-1">
                        <button type="submit" class="btn btn-success">Add Facility</button>

                        <a class="btn btn-secondary" href="/allfacility" role="button">See All Facility Data</a>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
@endsection()