@extends('/userdashboard')

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.5/b-2.2.2/b-colvis-2.2.2/b-html5-2.2.2/b-print-2.2.2/datatables.min.css" />
@endpush

@section('kontenuser')
<div class="row">
    <div class="col-md-6 col-sm-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>Add Bookings Data<small>exploria</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table border="1" class="table table-bordered table-striped" id="allhotel">
                    <thead>
                        <tr>
                            <th>Nama Hotel</th>
                            <th>Harga</th>
                            <th>Kota</th>
                            <th>Fasilitas</th>
                            <th>Catatan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @isset($listhotel)
                        @foreach($listhotel as $item=>$value)
                        <tr>
                            <td>{{$value->nama_hotel}}</td>
                            <td>{{$value->harga_hotel}}</td>
                            <td>{{$value->kota_hotel}}</td>
                            <td>{{$value->nama_fasilitas}}</td>
                            <td>{{$value->keterangan_hotel}}</td>
                            </form>
                        </tr>
                        @endforeach
                        @endisset
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>Add Bookings Data<small>exploria</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @if (session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
                @endif
                <form action="/addbookings" method="POST">
                    <p>For adding bookings data in <code>exploria</code> correctly at <a href="form.html"> form page</a></p>
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="nama_pengunjung">Visitors Name</label>
                            <input type="text" class="form-control" id="nama_pengunjung" name="nama_pengunjung" required="required" value="{{Auth::user()->name}}">
                        </div>
                        <div class="form-group">
                            <label for="nama_hotel">Hotel</label>
                            <input type="text" class="form-control" id="nama_hotel" name="nama_hotel" list="listhotel" required="required">
                            <datalist id="listhotel">
                                @isset($listhotel)
                                @foreach($listhotel as $item=>$value)
                                <option value="{{$value->nama_hotel}}">
                                    @endforeach
                                    @endisset
                        </div>
                        <div class="form-group">
                            <br>
                            <label for="malam">How Night?</label>
                            <select class="form-select" aria-label="Default select example" id="malam" name="malam" required="required">
                                <option selected>Select /Night</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">else (write in notes)</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="no_telp">No Telp</label>
                            <input type="text" class="form-control" id="no_telp" name="no_telp" required="required">
                        </div>
                        <div class="form-group">
                            <label for="alamat">Address</label>
                            <input type="text" class="form-control" id="alamat" name="alamat" required="required">
                        </div>
                        <div class="form-group">
                            <label for="keterangan_pengunjung">Notes</label>
                            <br>
                            <textarea id="keterangan_pengunjung" name="keterangan_pengunjung" required="required" rows="3"></textarea>
                        </div>
                    </div>

                    <div class="ml-3 mr-3 mb-3">
                        <button type="submit" class="btn btn-success"> Add Bookings</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection()

@push('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.5/b-2.2.2/b-colvis-2.2.2/b-html5-2.2.2/b-print-2.2.2/datatables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#allhotel').DataTable();
    });
</script>
<script>
    $('#allhotel').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'pdf', 'print', 'colvis'
        ]
    });
</script>
@endpush