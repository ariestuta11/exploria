@extends('/userdashboard')

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.5/b-2.2.2/b-colvis-2.2.2/b-html5-2.2.2/b-print-2.2.2/datatables.min.css" />
@endpush

@section('kontenuser')
<div class="clearfix"></div>

<div class="row m-5">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Invoice <small>Please Print and Show in Hotel</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @isset($listbookings)
                @foreach($listbookings as $item=>$value)
                <section class="content invoice">
                    <!-- title row -->
                    <div class="row">
                        <div class="  invoice-header">
                            <h1>
                                <i class="fa fa-globe"></i> Invoice.
                            </h1>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            From
                            <address>
                                <strong>Exploria Admin.</strong>
                                <br>Universitas Brawijaya
                                <br>Malang, Jawa Timur
                                <br>0878-6598-7624
                                <br>Email: exploria@gmail.com
                            </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            To
                            <address>
                                <strong>{{Auth::user()->name}}</strong>
                                <br>{{$value->alamat}}
                                <br>{{$value->no_telp}}
                            </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            <b>Invoice {{$value->id_pengunjung}}</b>
                            <br>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- Table row -->
                    <div class="row">
                        <div class="  table">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Night</th>
                                        <th>Hotel Name</th>
                                        <th>Name</th>
                                        <th>No Telp</th>
                                        <th style="width: 40%">Notes</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{$value->malam}}</td>
                                        <td>{{$value->nama_hotel}}</td>
                                        <td>{{$value->nama_pengunjung}}</td>
                                        <td>{{$value->no_telp}}</td>
                                        <td>{{$value->keterangan_pengunjung}} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-md-6">
                            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                Payment transaction to do it in hotel place, in notes: bring your Invoice!
                            </p>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-6">

                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    @endforeach
                    @endisset
                    <!-- this row will not appear when printing -->
                    <div class="row no-print">
                        <div class=" ">
                            <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                            <button class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit</button>

                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
@endsection()