<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class userController extends Controller
{
    public function index()
    {
        return view('user');
    }

    public function userhotel()
    {
        $listhotel = DB::table('hotel')->get();
        return view('userhotel', compact('listhotel'));
    }
    public function invoice($id_pengunjung)
    {
        $pilihpengunjung = DB::table('bookings')->where('id_pengunjung', $id_pengunjung)->last();
        return view('invoice', compact('pilihpengunjung'));
    }
}
